# Abstract

- turing machine is turing complete
- ...but lacks important primitives
- the memory is finite, programs need to interoperate, IO

# Linux, Unix, Unix-like & POSIX

- Unix - some specific OS with good concepts
- POSIX - standard set of OS interfaces
- Linux - de-facto popular, some good concepts

# Core concepts

- everything is a file (pipe, named pipe, file, devfs/FUSE)
- single-root filesystem, mounts
- processes: glorifed threads?
- [syscalls](https://man7.org/linux/man-pages/man2/syscalls.2.html) - the way to communicate with OS and other processes
- subprocess - fork/exec

...
