- write kill using syscalls (only the following form have to be supported: `kill -n $SIGNUM $PID`)
- bind-mount /dev/cpu to ~/mycpu, check contents, unmount
- write function to open subprocess using fork and exec

# How to call a syscall from Python

First check the syscall manpage to get it's signature

```bash
man 2 getcwd
```

```python
import ctypes

libc = ctypes.CDLL(None)

# manually create arrays to store returned strings
cwd_string = ctypes.ARRAY(ctypes.c_char, 1024)()

# call the function
ret_code = libc.getcwd(cwd_string, len(cwd_string))
assert ret_code # is not NULL

print(ret_code)
print(cwd_string.value.decode('ascii'))
```

# Write program that outputs only last 10 lines of it's stdin (tail)

Write two versions - the simplest one, and the one that is using a constant amount of RAM (never stores more than 10 lines)

### Reading from stdin:

```python
import sys
for x in sys.stdin:
    print(repr(x)))
```
